import interfaces.Block;
import models.Bloczek;
import interfaces.CompositeBlock;
import interfaces.Structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Wall implements Structure, CompositeBlock {

    public List<Bloczek> blocks = new ArrayList<Bloczek>();
    public Bloczek foundByColor;
    public List<Bloczek> foundBlocks = new ArrayList<Bloczek>();
    public List<Bloczek> foundBlocksByMaterial = new ArrayList<Bloczek>();
    private List<String> processed = new ArrayList<String>();
    private List<String> processedByMaterial = new ArrayList<String>();
    public List<String> duplicates = new ArrayList<String>();
    @Override
    public Optional<Block> findBlockByColor(String color) {

        for(Bloczek b : blocks) {
            recursiveSearch(blocks,b.getId(), color, "");
            processed.add(b.getId());
            if(foundBlocks.size() > 0) {
                break;
            }
        }

        if(foundBlocks.size() > 0) {
            return Optional.ofNullable(foundBlocks.get(0));
        }
        return null;
    }


    @Override
    public List<Bloczek> findBlocksByMaterial(String material) {

        for(Bloczek b : blocks) {
            recursiveSearch(blocks,b.getId(), "", material);
            processedByMaterial.add(b.getId());
        }

        return foundBlocksByMaterial;

    }

    @Override
    public int count() {
        return foundBlocksByMaterial.size();
    }

    @Override
    public List<Block> getBlocks() {
        return null;
    }

    public void initializeBlocksTree() {

        blocks.add(new Bloczek("1", "Czarny", "Tynk", ""));
        blocks.add(new Bloczek("2", "Biały", "kartongips", "1"));
        blocks.add(new Bloczek("3", "Czerwony", "drewno","1"));
        blocks.add(new Bloczek("4", "Brązowy", "Tynk","2"));
        blocks.add(new Bloczek("5", "Niebieski", "kartongips",""));
        blocks.add(new Bloczek("6", "Zielony", "metal", "5"));
        blocks.add(new Bloczek("7", "Żółty", "Tynk","6"));
        blocks.add(new Bloczek("8", "Fioletowy", "kartongips","7"));
        blocks.add(new Bloczek("9", "Zielony1", "blacha","8"));
        blocks.add(new Bloczek("10", "Karmazynowy", "szkło",""));
        blocks.add(new Bloczek("11", "Zielony2", "szkło","9"));
        blocks.add(new Bloczek("12", "Zielony3", "szkło","11"));

    }

    private void recursiveSearch(List<Bloczek> blockList, String id, String color, String material) {

        if(color != null && !color.isEmpty()) {
            for (int i=0; i<blockList.size();i++) {

                if(processed.contains(blockList.get(i).getId())){
                    continue;
                }

                if(blockList.get(i).getColor().equals(color) && !foundBlocks.contains(blockList.get(i))) {
                    foundBlocks.add(blockList.get(i));
                }

                if(!blockList.get(i).getParentId().isEmpty() && Integer.parseInt(blockList.get(i).getParentId()) > 0 && blockList.get(i).getId().equals(id)) {
                    recursiveSearch(blockList, blockList.get(i).getParentId(), color, "");
                } else {
                    // powrót poziom wyżej
                    return;
                }
            }
        } else if(material != null && !material.isEmpty()) {
            for (int i=0; i<blockList.size();i++) {

                if(processedByMaterial.contains(blockList.get(i).getId())){
                    continue;
                }

                if(blockList.get(i).getMaterial().equals(material) && !foundBlocksByMaterial.contains(blockList.get(i))) {
                    foundBlocksByMaterial.add(blockList.get(i));
                }

                if(!blockList.get(i).getParentId().isEmpty() && Integer.parseInt(blockList.get(i).getParentId()) > 0 && blockList.get(i).getId().equals(id)) {
                    recursiveSearch(blockList, blockList.get(i).getParentId(), "", material);
                } else {
                    // powrót poziom wyżej
                    return;
                }
            }
        }

    }

    public void checkColorRepeatability(List<Bloczek> blocks) {

        for(int i=0; i<blocks.size(); i++) {

            boolean flag = false;

            for(int j=0; j<blocks.size(); j++) {
                if(blocks.get(i).getId().equals(blocks.get(j).getId())) { // pominięcie sprawdzanego elementu listy
                    continue;
                }

                if(blocks.get(i).getColor().equals(blocks.get(j).getColor())) {
                    flag = true;
                }

            }

            if(flag && !duplicates.contains(blocks.get(i).getColor())) duplicates.add(blocks.get(i).getColor());
        }

    }
}
