import models.Bloczek;

import java.util.ArrayList;
import java.util.Optional;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Wall wall = new Wall();
        wall.initializeBlocksTree();

        String color = "Żółty";
        String material = "szkło";

        wall.checkColorRepeatability(wall.blocks);

        if(wall.duplicates.size() > 0) {
            System.out.println("Występują duplikaty następujących kolorów:");
            System.out.println();
            wall.duplicates.forEach(bloczek -> {
                System.out.println(bloczek);
            });
            System.out.println();
            System.out.println("Koniec");
            return;
        }

        var foundColor = wall.findBlockByColor(color);

        if(foundColor != null && Optional.ofNullable(foundColor).isPresent() ) {
            System.out.println("Znaleziono blok o kolorze:  "+color);
            System.out.println(foundColor.get().getId() + " - " + foundColor.get().getColor() + " - " + foundColor.get().getMaterial());
        } else {
            System.out.println("Nie znaleziono bloku o kolorze:  "+color);
        }

        System.out.println("Szukam bloków wykonanych z materiału: "+material);
        var bloczekFound = wall.findBlocksByMaterial(material);

        if(bloczekFound.size() > 0) {
            System.out.println("Ilość znalezionych bloków: "+wall.count());
            wall.foundBlocksByMaterial.forEach(bloczek -> {
                System.out.println(bloczek.getId() + " - " + bloczek.getColor() + " - " + bloczek.getMaterial());
            });
        } else {
            System.out.println("Nie znaleziono bloków wykonanych z: "+material);
        }

    }
}