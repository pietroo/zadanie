package interfaces;

public interface Block {
    String getId();
    String getColor();
    String getMaterial();
}
