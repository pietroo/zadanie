package models;

import interfaces.Block;

import java.util.List;

public class Bloczek implements Block {

    public String id;
    public String color;
    public String material;
    public String parentId;

    public Bloczek(String id, String color, String material, String parentId) {
        this.id = id;
        this.color = color;
        this.material = material;
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getMaterial() {
        return material;
    }

    public String getParentId() {
        return parentId;
    }
}
